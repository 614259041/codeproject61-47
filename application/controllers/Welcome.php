<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('Customer_model','cm');
	}

	//http://localhost/codeproject/index.php/welcome/index
	public function index()
	{
		$this->cm->showName();
		//$this->load->view('welcome_message');
	}
    //http://localhost/codeproject/index.php/welcome/show
	public function show(){
		$this->cm->showName();		
	}
	//http://localhost/codeproject/index.php/welcome/showfood
	public function showFood(){
		$this->load->model('Food_model','fm');
		$result = $this->fm->getFood()->result();
		print_r($result);
	}
	public function showFoodv2(){
		$this->load->model('Food_model','fm');
		$query = $this->fm->getFood()->result_array();
		$data['query'] = $query;

		// $this->load->database();
		// $sql = "SELECT * FROM food";
		// $query = $this->db->query($sql)->result_array();
		// $data['query'] = $query;

		// foreach($query as $row){
		// 	echo $row['fid']." ".$row['fname']." ".$row['fprice']." ".$row['famount']."</br>";
		// }

		// $data['query'] =array(
		//  'fid' => '1',
		//  'fname' => 'หิวข้าว',
		//  'fprice' => 'ไม่มีตังค์จ่าย',
		//  'famount' => 'ไม่จำกัด' 
		// );
		
		$this->load->view('food_list',$data);
	}
}
